<?php
session_start();

//jika belum login, alihkan ke login
if (empty($_SESSION['nama'])) {
  header('Location: login.php');
  exit();
}

// ... ambil data dari database
include 'prosespersen.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Dashboard</title>
  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Page Wrapper -->
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-book-open"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin</div>
      </a>
      <!-- Sidebar -->

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Divider -->

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <!-- Nav Item - Dashboard -->

      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Divider -->

      <!-- Heading -->
      <div class="sidebar-heading">
        User
      </div>
      <!-- Heading -->

      <!-- Nav Item - Warehouse -->
      <li class="nav-item">
        <a class="nav-link" href="datarps.php">
          <i class="fas fa-book"></i>
          <span>Data Rps</span></a>
      </li>
      <!-- Nav Item - Warehouse -->

      <!-- Nav Item - Produk -->
     <li class="nav-item">
                <a class="nav-link" href="simpus1.php">
                    <i class="fas fa-book"></i>
                    <span>Data Simpus</span></a>
                  </li>
      <li class="nav-item">
        <a class="nav-link" href="persen.php">
          <i class="fas fa-percent"></i>
          <span>Data Persentase</span></a>
      </li>
      <li class="nav-item">
                <a class="nav-link" href="matkul.php">
                    <i class="fas fa-plus"></i>
                    <span>Tambah Mata Kuliah</span></a>
            </li>
      <!-- Nav Item - Produk -->
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Divider -->

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
      <!-- Sidebar Toggler (Sidebar) -->

    </ul>
    <!-- Sidebar Toggler (Sidebar) -->
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Content Wrapper -->

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto" style="background-color: white;">
            <div class="topbar-divider d-none d-sm-block"></div>
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown">
                <p class="mt-3"> Admin </p>
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in">
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="container-fluid">

            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
              <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            </div>
            <div class="row">
              <!-- Page Heading -->

              <!-- Kopi -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Rps</div>
                        <div class="h5 mb-4 font-weight-bold text-gray-800"><?= $banyakrps ?> Buku</div>
                        <a class="btn btn-primary" id="kopi" href="datarps.php" data-target="tooltip" title="Yeay!">Lihat</a>
                      </div>
                      <div class="col-auto">
                        <img src="img/book.png" width="60px" height="60px">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Kopi -->

              <!-- Barang -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Data Simpus</div>
                        <div class="h5 mb-4 font-weight-bold text-gray-800"><?= $banyaksimpus ?> Buku</div>
                        <a class="btn btn-primary" id="kopi" href="datasimpus.php" data-target="tooltip" title="Yeay!">Lihat</a>
                      </div>
                      <div class="col-auto">
                        <img src="img/book1.png" width="60px" height="60px" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Barang -->

              <!-- Laporan -->
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Persentase Buku</div>
                        <div class="h5 mb-4 font-weight-bold text-gray-800"><?= $hasilpersen ?>%</div>
                        <a class="btn btn-primary" id="kopi" href="persen.php" data-target="tooltip" title="Yeay!">Lihat</a>
                      </div>
                      <div class="col-auto">
                        <img src="img/persen.png" width=80px" height="80px" alt="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Laporan -->

              <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
          </div>
          <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
          <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" name="logoutModal" id="logoutModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
              <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.php">Logout</a>
              </div>
            </div>
          </div>
        </div>

        <!-- Title Button Lihat-->
        <script>
          $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>
        <!-- Title Button Lihat-->

        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="vendor/chart.js/Chart.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/chart-area-demo.js"></script>
        <script src="js/demo/chart-pie-demo.js"></script>

</body>

</html>