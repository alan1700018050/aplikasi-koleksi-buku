<?php
session_start();

// jika sudah login, alihkan ke halaman list
if (isset($_SESSION['user'])) {
  header('Location: home.php');
  exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin - Login</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-login">
  <div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-lg-7">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Halaman Login!</h1>
                  </div>
                  <form action="" method="POST" class="user">
                    <div class="form-group">
                      <input type="text" name="username" class="form-control form-control-user" placeholder="Username">
                    </div>
                    <div class="form-group">
                      <input type="password" name="pass" class="form-control form-control-user mt-2" placeholder="Password">
                    </div>
                    <input name="login" class="btn btn-primary btn-user btn-block mt-2" type="submit" value="Login">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- PHP -->
  <?php
  if (isset($_POST['login'])) {
    include "connection2.php";
    $cek_data = mysqli_query($db, "SELECT * FROM user WHERE
          username = '" . $_POST['username'] . "' AND password = '" . $_POST['pass'] . "'");
    $data = mysqli_fetch_array($cek_data);
    $level = $data['level'];
    $nama = $data['nama_lengkap'];
    // Notifkasi Gagal Login
    if (mysqli_num_rows($cek_data) > 0) {
      session_start();
      $_SESSION['nama'] = $nama;
      if ($level == 'admin') {
        header('location:index.php');
      } elseif ($level == 'operator') {
        header('location:index2.php');
      }
    } else {
      echo 'Gagal Login';
    }
  }
  ?>
  <!-- PHP -->

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>
</html>